import io from "socket.io";
import { Message } from "./db/postgres/models";
import _ from "lodash";

import server from "./server";

const socketIO = io(server);

// const rooms = {};

socketIO.on("connection", (socket) => {
  socket.on("event://connect-to-room", async (data) => {
    const { room_id, user_id } = JSON.parse(data);
    // if (!_.has(rooms, "room_id")) {
    //   socket.join(room_id);
    //   rooms[room_id] = [user_id];
    // } else if (!rooms[room_id].includes(user_id)) {
    //   socket.join(room_id);
    //   rooms[room_id] = [...rooms[room_id], user_id];
    // }

    socket.broadcast.to(room_id).emit("event://success-connect", user_id);
  });

  socket.on("disconnect", (reason) => {
    console.log("DISCONNECT !!!!!!!", reason);
  });

  socket.on("event://send-message", async (msg) => {
    console.log("got", msg);

    try {
      const data = JSON.parse(msg);

      await Message.create(data);

      socket.broadcast.to(data.room_id).emit(
        "event://get-message",
        JSON.stringify({
          userId: data.from,
          message: data.body,
          room_id: data.room_id,
          recipientId: data.to,
        })
      );

      // socket.broadcast.emit(
      //   "event://get-message",
      //   JSON.stringify({
      //     userId: data.from,
      //     message: data.body,
      //     room_id: data.room_id,
      //     recipientId: data.to,
      //   })
      // );
    } catch (error) {
      console.error(error);
    }
  });
});

export default server;
