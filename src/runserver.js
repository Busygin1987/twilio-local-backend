import config from 'config';

import postgres from './db/postgres/postgres';
import serverSocket from './ws';

console.info('NODE_ENV', process.env.NODE_ENV);

const HOST = config.get('server.host');
const PORT = config.get('server.port');

postgres.authenticate()
  .then(() => {
  console.info('Connection to the database has been established successfully.');
  const instance = serverSocket.listen(PORT, HOST);
  instance.on('listening', () => console.info('Available on:', `${HOST}:${PORT}`));
  instance.on('error', (error) => console.error(error));
});
