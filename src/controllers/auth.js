import _ from 'lodash';

import { passwordHash } from '../utils/helpers';
import { User } from '../db/postgres/models';
import createJwt from '../utils/create-jwt';

const USER_FIELDS = [
  'firstName',
  'lastName',
  'email',
  'username',
  'facebookId'
];

export const loginByFB = (ctx) => {
  const { user } = ctx.state;
  const token = createJwt(user);

  ctx.status = 308;
  ctx.redirect(`twilio://login?token=${token}&user=${JSON.stringify(user)}`);
}


export const login = async (ctx) => {
  const { email, username, password } = ctx.request.body;

  let user;
  if (email) {
    user = await User.findByEmail(email);
  } else {
    user = await User.findByUsername(username);
  }
  ctx.assert(user, 404, 'User not found');

  const equals = await passwordHash.compare(password, user.password);
  ctx.assert(equals, 403, 'The email address or password is incorrect.');

  const token = createJwt(user);

  ctx.body = { token: `Bearer ${token}` };
};

export const signUp = async (ctx) => {
  const { email, password } = ctx.request.body;

  let user = await User.findByEmail(email);

  if (!user) {
    const hash = await passwordHash.create(password);
    const userData = _.pick(ctx.request.body, USER_FIELDS);

    try {
      user = await User.create(_.assign(userData, { password: hash }));
    } catch (error) {
      const errorMessage = error.message || error;

      if (errorMessage === 'Validation error') {
        return ctx.status = 422;
      } else {
        return ctx.status = 500;
      }
    }

    const token = createJwt(user);

    ctx.body = { token: `Bearer ${token}` };
  } else {
    ctx.throw(400, 'Email already exist! Please, login!');
  }
};

export const logout = async (ctx) => {
  // const user = ctx.state.user;

  // const { revoke_all: revokeAll } = ctx.query;

  // if (revokeAll) {
  //   await redisJwt.removeAll(user.id);
  // } else {
  //   await redisJwt.remove(ctx.state.auth.jti);
  // }

  // ctx.status = 204;
};
