import _ from "lodash";
import fs from "fs";
import { resolve } from "path";

import { User, Friend, Photo } from "../db/postgres/models";
import createJwt from "../utils/create-jwt";

const USER_FIELDS = [
  "id",
  "firstName",
  "lastName",
  "email",
  "username",
  "avatar",
  "facebookId",
  "album",
];

export const updateUserAvatar = async (ctx) => {
  ctx.assert(ctx.request.is("multipart/*"), 400);

  const { id } = ctx.request.body;

  const user = await User.getById(id);
  ctx.assert(user, 404, "User not found");

  const photo = await Photo.create({
    type: ctx.file.mimetype,
    name: ctx.file.originalname,
    data: fs.readFileSync(
      `${resolve(__dirname, "..", "..", "assets", "uploads")}/${
        ctx.file.filename
      }`
    ),
    user_id: id,
  });

  if (photo) {
    fs.writeFileSync(
      `${resolve(__dirname, "..", "..", "assets", "tmp")}/${photo.name}`,
      photo.data
    );
  }

  const result = await user.update({
    avatar: photo.id,
  });
  ctx.assert(result, 422);

  ctx.body = { user: result };
};

export const getUserFriends = async (ctx) => {
  const user = ctx.state.user;
  const result = [];

  const userWithFriends = await User.getFriends(user.id);
  ctx.assert(userWithFriends, 404, "User not found!");

  if (userWithFriends.friends && userWithFriends.friends.length > 0) {
    for (const friend of userWithFriends.friends) {
      const userFriend = await User.getFriendById(friend.friend_id);
      ctx.assert(userFriend, 404, "Friend not found");
      userFriend.dataValues.roomId = friend.id;
      result.push(userFriend);
    }
  }

  if (userWithFriends.chums && userWithFriends.chums.length > 0) {
    for (const chum of userWithFriends.chums) {
      const userFriend = await User.getFriendById(chum.user_id);
      ctx.assert(userFriend, 404, "Friend not found");
      userFriend.dataValues.roomId = chum.id;
      result.push(userFriend);
    }
  }

  ctx.body = { friends: result };
};

export const getUser = async (ctx) => {
  const user = ctx.state.user;
  const result = await User.getAllUsersDataById(user.id);
  ctx.assert(result, 404, "User not found");

  ctx.body = { user: _.pick(result, USER_FIELDS) };
};

export const createUser = async (ctx) => {
  const { user: userData } = ctx.request.body;
  let user;

  if (userData.facebookId) {
    user = await User.findByFacebookId(userData.facebookId);
  } else {
    user = await User.findByEmail(userData.email);
  }

  if (!user) {
    user = await User.create(userData);
  }
  const token = createJwt(user);

  ctx.body = { token: `Bearer ${token}`, user };
};

export const getAllUsers = async (ctx) => {
  const user = ctx.state.user;
  const users = await User.findAllUsers(user.id);
  ctx.assert(users, 404, "User not found");

  ctx.body = { users: users.map((user) => _.pick(user, USER_FIELDS)) };
};

export const addNewFriend = async (ctx) => {
  const user = ctx.state.user;
  const { friendId } = ctx.request.body;

  const inFriend = await Friend.checkIsFriend(user.id, friendId);

  if (!inFriend) {
    const friend = await Friend.create({
      user_id: user.id,
      friend_id: friendId,
    });
    ctx.assert(friend, 422);

    ctx.body = { roomId: friend.id };
  } else {
    ctx.body = { roomId: inFriend.id };
  }
};

export const deleteFriend = async (ctx) => {
  const user = ctx.state.user;
  const { id } = ctx.params;

  await Friend.deleteById(id, user.id);

  ctx.status = 204;
};
