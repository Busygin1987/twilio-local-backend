import _ from "lodash";
import { Photo } from "../db/postgres/models";

export const getPhoto = async (ctx) => {
  const { id } = ctx.params;
  const photo = await Photo.getById(id);

  ctx.assert(photo, 404, "Photo not found");

  ctx.body = photo.data;
};
