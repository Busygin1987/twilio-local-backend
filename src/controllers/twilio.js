import twilio, { jwt } from 'twilio';
import config from 'config';

const { VideoGrant } = jwt.AccessToken;

const MAX_ALLOWED_SESSION_DURATION = 14400;
const ACCOUNT_SID = config.get('twilio.twilioAccountSid');
const API_KEY_SID = config.get('twilio.twilioApiKeySID');
const API_KEY_SECRET = config.get('twilio.twilioApiKeySecret');
const APP_AUTH_TOKEN = config.get('twilio.twilioAuthToken');

export const getTwilioToken = async (ctx) => {
  const { identity, room } = ctx.query;

  const token = new jwt.AccessToken(ACCOUNT_SID, API_KEY_SID, API_KEY_SECRET, {
    ttl: MAX_ALLOWED_SESSION_DURATION,
  });
  token.identity = identity;
  const videoGrant = new VideoGrant({ room });

  token.addGrant(videoGrant);

  console.log(`issued token for ${identity} in room ${room}`);

  ctx.body = { token: token.toJwt() };
};

export const createTwilioRoom = async (ctx) => {
  const { name, type } = ctx.request.body;

  const client = twilio(ACCOUNT_SID, APP_AUTH_TOKEN);

  try {
    await client.video.rooms(name).fetch();
    ctx.body = { message: 'Room already exists' };
  } catch {
    const room = await client.video.rooms.create({
      enableTurn: true,
      recordParticipantsOnConnect: true,
      statusCallback: 'http://example.org',
      type: type,
      uniqueName: name
    });

    ctx.body = { room };
  }
};

export const updateTwilioRoom = async (ctx) => {
  const { roomSid } = ctx.params;
  const { status } = ctx.query;

  const client = twilio(ACCOUNT_SID, APP_AUTH_TOKEN);

  if (status !== 'completed') {
    ctx.status = 400;
  }
  const result = await client.video.rooms(roomSid).update({ status });
  ctx.assert(result, 500);

  ctx.status = 200;
};

export const getListTwilioRooms = async (ctx) => {
  const { status } = ctx.query;
  const client = twilio(ACCOUNT_SID, APP_AUTH_TOKEN);

  const rooms = await client.video.rooms.list({ status, limit: 20 });
  ctx.assert(rooms, 404);

  ctx.body = { rooms };
};