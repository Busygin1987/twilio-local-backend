import _ from 'lodash';
import { Message } from '../db/postgres/models';

const MESSAGE_FIELDS = [
  'body',
  'to'
];

export const getMessages = async (ctx) => {
  const { recipient_id } = ctx.params;
  const user = ctx.state.user;
  const { limit = 10, offset = 1, dir = 'DESC' } = ctx.query;
  const messages = await Message.getChatMessages(user.id, recipient_id, limit, offset, dir);
  ctx.assert(messages, 404, 'Messages not found');

  ctx.body = { messages: messages.map(message => message), count: messages.length };
};

export const getMessage = async (ctx) => {
  const { id } = ctx.params;
  const message = await Message.getMessageById(id);
  ctx.assert(message, 404, 'Message not found');

  ctx.body = { message };
};

export const createMessage = async (ctx) => {
  const user = ctx.state.user;
  const data = _.pick(ctx.request.body, MESSAGE_FIELDS);
  const message = await Message.create(_.assign(data, { from: user.id }));
  ctx.assert(message, 422);

  ctx.status = 201;
};

export const updateMessage = async (ctx) => {
  const { body, id } = ctx.request.body;
  const message = await Message.update(
    { body },
    { returning: true, where: { id } }
  );
  ctx.assert(message, 422);

  ctx.body = { message };
};

export const deleteMessage = async (ctx) => {
  const { id } = ctx.params;
  const result = await Message.destroy({
    where: { id }
  });

  const user = ctx.state.user;
  const messages = await Message.getMessagesByUserId(user.id);
  ctx.assert(messages, 404, 'Messages not found');

  ctx.body = { text: `Message has been deleted!` };
};