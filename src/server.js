import Koa from "koa";
import body from "koa-bodyparser";
import convert from "koa-convert";
import logger from "koa-logger";
import json from "koa-json";
import cors from "koa-cors";
import http from "http";
import serve from "koa-static";

import errorHandler from "./middleware/error-handler";
import passportJwt from "./middleware/passport-jwt-init";
import passportFB from "./middleware/passport-fb-init";

import routes from "./routes";

const app = new Koa();
const server = http.createServer(app.callback());

app.use(logger());
app.use(errorHandler());
app.use(convert(cors()));
app.use(serve("./public/uploads"));
app.use(passportFB);
app.use(passportJwt);
app.use(json());
app.use(body({ formLimit: "100mb", jsonLimit: "16mb" }));
app.use(routes);

export default server;
