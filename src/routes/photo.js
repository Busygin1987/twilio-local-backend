import Router from "koa-router";
import * as ctrl from "../controllers/photo";
import passport from "koa-passport";

const router = new Router();

router.get(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  ctrl.getPhoto
);

export default router.routes();
