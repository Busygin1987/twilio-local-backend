import Router from 'koa-router';
import * as ctrl from '../controllers/auth';
import passport from 'passport';

const router = new Router();

router
  .get('/facebook', passport.authenticate('facebook', { scope: ['email', 'public_profile'], session: false }))
  .get('/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/' }), ctrl.loginByFB)
  .post('/sign-in', ctrl.login)
  .post('/sign-up', ctrl.signUp)
  .get('/logout', ctrl.logout);

export default router.routes();