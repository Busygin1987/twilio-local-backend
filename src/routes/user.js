import Router from "koa-router";
import passport from "koa-passport";

import * as ctrl from "../controllers/user";
import upload from "../middleware/multer";

const router = new Router();

router
  .get("/", passport.authenticate("jwt", { session: false }), ctrl.getUser)
  .post("/", ctrl.createUser)
  .put(
    "/",
    passport.authenticate("jwt", { session: false }),
    upload.single("photo"),
    ctrl.updateUserAvatar
  )
  .get(
    "/friends",
    passport.authenticate("jwt", { session: false }),
    ctrl.getUserFriends
  )
  .get(
    "/all",
    passport.authenticate("jwt", { session: false }),
    ctrl.getAllUsers
  )
  .delete(
    "/friend/:id",
    passport.authenticate("jwt", { session: false }),
    ctrl.deleteFriend
  )
  .post(
    "/friend",
    passport.authenticate("jwt", { session: false }),
    ctrl.addNewFriend
  );

export default router.routes();
