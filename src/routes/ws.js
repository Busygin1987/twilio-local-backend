import Router from "koa-router";

import { Message } from "../db/postgres/models";

const router = new Router();

router.get("/room/:roomId", async (ctx) => {
  const { roomId } = ctx.params;
  const { limit, offset } = ctx.query;

  const result = await Message.getMessageByRoomId(roomId, limit, offset);
  ctx.assert(result, 404, "Message not found");

  const messages = result.map((message) => ({
    userId: message.from,
    message: message.body,
    room_id: message.room_id,
  }));

  const response = {
    room: roomId,
    chats: messages,
  };

  ctx.body = { data: response };
});

export default router.routes();
