import config from 'config';
import jwt from 'jsonwebtoken';
import { v4 as uuidv4 } from 'uuid';

const JWT_SECRET = config.get('jwt.secret');
const JWT_EXPIRES_IN = config.get('jwt.expiresIn');

export default (user) => {
  const payload = user.publish();
  const options = { expiresIn: JWT_EXPIRES_IN , subject: `${user.id}`, jwtid: uuidv4() };
  return jwt.sign(payload, JWT_SECRET, options);
};
