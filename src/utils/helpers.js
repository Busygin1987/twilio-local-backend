import bcrypt from 'bcryptjs';

export const validateEmail = email => {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

export const passwordHash = {
  create: async (password) => {
    const salt = bcrypt.genSaltSync(10);
    return await bcrypt.hash(password, salt);
  },
  compare: async (password, hash) => await bcrypt.compare(password, hash)
};
