import User from "./user";
import Message from "./message";
import Friend from "./friend";
import Photo from "./photo";

User.hasMany(Message, {
  as: "messages",
  foreignKey: "id",
  onDelete: "CASCADE",
});
Message.belongsTo(User, { as: "author", foreignKey: "from" });
Message.belongsTo(User, { as: "recipient", foreignKey: "to" });
User.hasMany(Friend, {
  as: "friends",
  foreignKey: "user_id",
  onDelete: "CASCADE",
});
User.hasMany(Friend, {
  as: "chums",
  foreignKey: "friend_id",
  onDelete: "CASCADE",
});
Friend.belongsTo(User, { as: "friends", foreignKey: "id" });
User.hasMany(Photo, {
  onDelete: "CASCADE",
  as: "album",
});
Photo.belongsTo(User, { foreignKey: "user_id", as: "author" });

export { User, Message, Friend, Photo };
