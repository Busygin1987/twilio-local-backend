import _ from "lodash";
import Sequelize, { Op } from "sequelize";
import sequelize from "../postgres";
import { Friend } from "./index";

const Model = Sequelize.Model;

const user = [
  "id",
  "firstName",
  "lastName",
  "username",
  "email",
  "facebookId",
  "avatar",
];

export default class User extends Model {
  static getFriends(id) {
    return this.findOne({
      where: { id },
      include: [
        {
          model: Friend,
          as: "friends",
          required: false,
          order: [["createdAt", "ASC"]],
        },
        {
          model: Friend,
          as: "chums",
          required: false,
          order: [["createdAt", "ASC"]],
        },
      ],
    });
  }

  static getById(id) {
    return this.findOne({
      where: { id },
      attributes: { exclude: ["password"] },
    });
  }

  static getAllUsersDataById(id) {
    return this.findOne({
      where: { id },
      attributes: { exclude: ["password"] },
      include: ["album"],
    });
  }

  static getFriendById(id) {
    return this.findOne({
      where: { id },
      attributes: { exclude: ["password"] },
    });
  }

  static findByEmail(email) {
    return this.findOne({
      where: { email: email.toLowerCase() },
    });
  }

  static findByFacebookId(id) {
    return this.findOne({
      where: { facebook_id: id },
    });
  }

  static findByUsername(username) {
    return this.findOne({
      where: { username: username.toLowerCase() },
    });
  }

  static findAllUsers(id) {
    return this.findAll({
      where: {
        id: {
          [Op.not]: id,
        },
      },
    });
  }

  publish() {
    return _.pick(this, user);
  }
}

User.init(
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true,
    },
    firstName: {
      type: Sequelize.STRING,
      field: "first_name",
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    lastName: {
      type: Sequelize.STRING,
      field: "last_name",
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    username: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    facebookId: {
      type: Sequelize.STRING,
      allowNull: true,
      field: "facebook_id",
    },
    email: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    password: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    avatar: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    createdAt: {
      allowNull: true,
      field: "created_at",
      type: Sequelize.DATE,
    },
    updatedAt: {
      field: "updated_at",
      type: Sequelize.DATE,
    },
    deletedAt: {
      field: "deleted_at",
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: true,
    paranoid: true,
    hooks: {
      beforeCreate: function (user) {
        if (user.email) {
          _.set(user, "email", user.email.toLowerCase());
        }
        if (user.username) {
          _.set(user, "username", user.username.toLowerCase());
        }
      },
      beforeUpdate: function (user) {
        if (user.email) {
          _.set(user, "email", user.email.toLowerCase());
        }
        if (user.username) {
          _.set(user, "username", user.username.toLowerCase());
        }
      },
    },
    sequelize,
    modelName: "user",
    tableName: "Users",
    freezeTableName: true,
  }
);
