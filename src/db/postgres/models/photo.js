import _ from "lodash";
import Sequelize from "sequelize";
import sequelize from "../postgres";

const Model = Sequelize.Model;

export default class Photo extends Model {
  static getById(id) {
    return this.findOne({
      where: { id },
    });
  }
}

Photo.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
    },
    user_id: {
      type: Sequelize.UUID,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    type: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    data: {
      type: Sequelize.BLOB,
      allowNull: false,
    },
    createdAt: {
      allowNull: true,
      field: "created_at",
      type: Sequelize.DATE,
    },
    updatedAt: {
      field: "updated_at",
      type: Sequelize.DATE,
    },
    deletedAt: {
      field: "deleted_at",
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: true,
    paranoid: true,
    sequelize,
    modelName: "photo",
    tableName: "Photos",
    freezeTableName: true,
  }
);
