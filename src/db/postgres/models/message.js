import _ from "lodash";
import Sequelize, { Op } from "sequelize";
import sequelize from "../postgres";

const Model = Sequelize.Model;

const user = ["id", "body", "from", "to"];

export default class Message extends Model {
  static getMessagesByUserId(id, limit = 10, offset = 1, dir = "ASC") {
    return this.findAll({
      where: {
        [Op.or]: [{ from: id }],
      },
      order: [["created_at", dir]],
      offset,
      limit,
    });
  }

  static getChatMessages(
    id,
    recipient_id,
    limit = 10,
    offset = 1,
    dir = "ASC"
  ) {
    return this.findAll({
      where: {
        [Op.or]: [{ from: id }, { to: recipient_id }],
      },
      order: [["created_at", dir]],
      offset,
      limit,
    });
  }

  static getMessageByRoomId(room_id, limit = 10, offset = 0, dir = "DESC") {
    return this.findAll({
      where: { room_id },
      attributes: { exclude: ["to", "id", "created_at", "updated_at"] },
      order: [["created_at", dir]],
      offset: offset * limit,
      limit,
    });
  }

  static getMessageById(id) {
    return this.findOne({
      where: { id },
    });
  }

  publish() {
    return _.pick(this, user);
  }
}

Message.init(
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true,
    },
    body: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    from: {
      type: Sequelize.UUID,
      field: "from",
      references: {
        model: "Users",
        key: "id",
      },
      allowNull: false,
    },
    to: {
      type: Sequelize.UUID,
      field: "to",
      references: {
        model: "Users",
        key: "id",
      },
      allowNull: false,
    },
    room_id: {
      type: Sequelize.UUID,
      allowNull: false,
    },
  },
  {
    timestamps: true,
    paranoid: true,
    sequelize,
    modelName: "message",
    tableName: "Messages",
    freezeTableName: true,
  }
);
