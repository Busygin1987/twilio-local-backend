import { User } from '../db/postgres/models';

export default jwtKey => async (ctx, next) => {
  const jwt = ctx.state[jwtKey];
  if (jwt) {
    const user = await User.getById(jwt.sub);
    ctx.assert(user, 401);

    ctx.state.user = user;
  }

  await next();
};
