import statuses from 'statuses';
import moment from 'moment';

export default () => async (ctx, next) => {
  try {
    await next();
  } catch (error) {
    console.error(`${moment()} ---------------------------------------------------->`, ctx.request.path);
    console.error('Error: ', error);

    ctx.status = error.statusCode || 500;
    ctx.message = ctx.status === 500 ? statuses.codes[ctx.status] : error.message || statuses.codes[ctx.status];
    ctx.body = { message: ctx.message };
  }
};