import _ from 'lodash';
import jwt from 'koa-jwt';
import config from 'config';

export default (skip) => {
  console.log('skip :', skip);
  return jwt({
    secret: config.get('jwt.secret'),
    key: 'auth',

    getToken(ctx) {
      const [scheme, credentials] = _.get(ctx, 'header.authorization', '').split(' ');
      return scheme === 'Bearer' ? credentials : null;
    },

    // isRevoked: async (ctx, { jti }) => !await redisJwt.exists(jti)
  }).unless(skip);
}
