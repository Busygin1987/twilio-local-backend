import multer from "@koa/multer";
import path, { resolve } from "path";

// "public/uploads"
const multerConfig = {
  storage: multer.diskStorage({
    destination: resolve(__dirname, "..", "..", "assets", "uploads"),
    filename: (req, file, cb) => {
      return cb(
        null,
        `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`
      );
    },
  }),
};

const checkFileType = (file, cb) => {
  const filetypes = /jpeg|jpg|png|gif/;
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);

  if (extname && mimetype) {
    cb(null, true);
  } else {
    cd(null, false);
  }
};

const uploadFile = multer({
  storage: multerConfig.storage,
  limits: {
    fileSize: 1024 * 1024 * 5,
  },
  fileFilter: (req, file, cb) => {
    checkFileType(file, cb);
  },
});

export default uploadFile;
