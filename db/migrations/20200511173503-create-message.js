'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Messages', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      body: {
        type: Sequelize.TEXT
      },
      from: {
        type: Sequelize.UUID,
        field: 'from',
        references: {
          model: 'Users',
          key: 'id'
        },
        allowNull: false
      },
      to: {
        type: Sequelize.UUID,
        field: 'to',
        references: {
          model: 'Users',
          key: 'id'
        },
        allowNull: false
      },
      room_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deleted_at: {
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Messages');
  }
};